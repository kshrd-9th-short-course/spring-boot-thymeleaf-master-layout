package com.chanchhaya.thymeleaflayout;

import com.chanchhaya.thymeleaflayout.model.Article;
import com.chanchhaya.thymeleaflayout.model.Category;
import com.chanchhaya.thymeleaflayout.model.Role;
import com.chanchhaya.thymeleaflayout.model.User;
import com.chanchhaya.thymeleaflayout.repository.jdbc.CategoryRepositoryImp;
import com.chanchhaya.thymeleaflayout.repository.mybatis.ArticleRepository;
import com.chanchhaya.thymeleaflayout.repository.mybatis.CategoryRepository;
import com.chanchhaya.thymeleaflayout.repository.mybatis.UserRepository;
import com.chanchhaya.thymeleaflayout.service.impl.ArticleServiceImpl;
import com.chanchhaya.thymeleaflayout.service.impl.CategoryServiceImpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;
import java.util.UUID;

@SpringBootTest
class ThymeleafLayoutApplicationTests {

    Log log = LogFactory.getLog(ThymeleafLayoutApplicationTests.class);

    @Autowired
    private CategoryServiceImpl categoryService;

    @Autowired
    private CategoryRepositoryImp categoryRepositoryImp;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleServiceImpl articleService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Test
    void generatePassword() {
        System.out.println("Password = " + passwordEncoder.encode("123"));
    }

    @Test
    void selectAllUsers() {
        List<User> users = userRepository.selectAllUsers();
        log.error("Users = " + users);
    }

    @Test
    void selectRoleByUserId() {
        List<Role> roles = userRepository.selectRoleByUserId(21);
        log.info("Roles = " + roles);
    }

    @Test
    void selectArticleByUUID() {
        System.out.println(articleService.findByUUID("0ca4792c-6dbd-46f2-a32d-e93952d450eb"));
    }

    @Test
    void selectArticle() {
        System.out.println(articleRepository.selectCategoryById(10));
        System.out.println(articleRepository.selectAll());
    }

    @Test
    void insertArticle() {
        Article article = new Article();
        article.setUuid(UUID.randomUUID().toString());
        article.setTitle("Testing Title");
        article.setDescription("Testing Description");
        article.setThumbnail("/img/testing-thumbnail.png");

        Category category = new Category();
        category.setId(11);
        category.setName("Science");

        article.setCategory(category);

        articleRepository.insert(article);
    }

    @Test
    void testService() {
        System.out.println(categoryService.findCategoryAndPrefixHRD());
    }

    @Test
    void selectAllCategories() {
        System.out.println(categoryRepository.selectAll());
    }

    @Test
    void contextLoads() {

        List<Category> categories = categoryRepositoryImp.findAll();

        System.out.println(categories);

    }

}
