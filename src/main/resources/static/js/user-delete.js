$(document).on('click', '.btn-delete', function(e) {
    e.preventDefault()

    let isConfirmed = confirm('Are you sure to delete this user?')

    if (isConfirmed) {
        $.ajax({
            url: 'http://localhost:8080/kshrd/admin/user/manage/delete-by-uuid/' + $(this).data('uuid'),
            method: 'DELETE',
            success: function(response) {
                console.log(response)
                findAllUsersWithTemplate()
            }, 
            error: function(error) {
                console.log(error)
            }
        })
    } else {
        alert('Operation is terminated')
    }
    
})