$(function () {
    findAllUsersWithTemplate()
})

function findAllUsersWithTemplate() {
    $.ajax({
        url: '/kshrd/user/list-data',
        method: 'GET',
        success: function (response) {
            console.log(response)
            $('.user-table').html(response)
        },
        error: function (error) {
            console.log(error)
        }
    })
}