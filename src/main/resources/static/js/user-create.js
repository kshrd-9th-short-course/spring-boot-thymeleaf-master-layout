let var_uuid = undefined

$(function() {
    console.log('hello')
})

$(document).on('click', '#btnCreateNewUser', function (e) {
    e.preventDefault()

    let user = {
        "username": $('#username').val(),
        "email": $('#email').val(),
        "password": $('#password').val(),
        "roles": [{
            "id": $('#role').val(),
            "name": $('#role option:selected').text()
        }]
    }

    console.log(user)

    $.ajax({
        url: '/kshrd/admin/user/manage/create-new-user',
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        data: JSON.stringify(user),
        success: function (response) {
            console.log(response)
            $('#modal-manage-user').modal('hide')
            findAllUsersWithTemplate()
            clearData()
        },
        error: function (error) {
            console.log(error)
        }
    })

})

$(document).on('click', '#btnSaveUpdate', function(e) {
    // update existing user
    e.preventDefault()

    let user = {
        "uuid": var_uuid,
        "username": $('#username').val(),
        "email": $('#email').val(),
        "password": $('#password').val(),
        "roles": [{
            "id": $('#role').val(),
            "name": $('#role option:selected').text()
        }]
    }

    $.ajax({
        url: '/kshrd/admin/user/manage/update-by-uuid',
        method: 'PUT',
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        data: JSON.stringify(user),
        success: function(response) {
            $('#btnSaveUpdate').text('Create new')
            $('#btnSaveUpdate').attr('id', 'btnCreateNewUser')
            $('#modal-manage-user').modal('hide')
            findAllUsersWithTemplate()
            clearData()
        },
        error: function(error) {
            console.log(error)
        }
    })

})

$(document).on('click', '.btn-edit', function(e) {
    e.preventDefault()
    
    $('#btnCreateNewUser').text('Save update')
    $('#btnCreateNewUser').attr('id', 'btnSaveUpdate')

    var_uuid = $(this).data('uuid')

    $.ajax({
        url: '/kshrd/admin/user/manage/find-one-by-uuid?uuid=' + var_uuid,
        method: 'GET',
        success: function(response) {
            prepareUserForUpdate(response)
        }, 
        error: function(error) {
            console.log(error)
        }
    })
})

function clearData() {
    $('#username').val('')
    $('#email').val('')
    $('#password').val('')
}

function prepareUserForUpdate(user) {
    $('#modal-manage-user').modal('show')
    $('#username').val(user.username)
    $('#email').val(user.email)
    $('#password').val(user.password)
    $('#role').val(user.roles[0].id)
}