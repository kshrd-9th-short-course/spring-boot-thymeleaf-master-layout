package com.chanchhaya.thymeleaflayout.controller;

import com.chanchhaya.thymeleaflayout.model.Article;
import com.chanchhaya.thymeleaflayout.model.Category;
import com.chanchhaya.thymeleaflayout.service.impl.ArticleServiceImpl;
import com.chanchhaya.thymeleaflayout.service.impl.CategoryServiceImpl;
import com.chanchhaya.thymeleaflayout.service.impl.ImageUploadServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
public class ArticleController {

    private final static String CREATE_MAPPING = "/admin/article/create";
    private final static String VIEW_ALL_MAPPING = "/admin/article/view-all";
    private final static String EDIT_MAPPING = "/admin/article/edit";

    private final static String CREATE_PAGE = "/admin/article/create";
    private final static String VIEW_ALL_PAGE = "/admin/article/view-all";

    private final ArticleServiceImpl articleService;
    private final CategoryServiceImpl categoryService;
    private final ImageUploadServiceImpl imageUploadService;

    @Autowired
    public ArticleController(ArticleServiceImpl articleService, CategoryServiceImpl categoryService, ImageUploadServiceImpl imageUploadService) {
        this.articleService = articleService;
        this.categoryService = categoryService;
        this.imageUploadService = imageUploadService;
    }

    @PostMapping(EDIT_MAPPING + "/{uuid}")
    public String edit(ModelMap modelMap, @ModelAttribute Article article,
                       @PathVariable("uuid") String uuid,
                       @RequestParam("file") MultipartFile file) throws IOException {

        String imageName = "";

        System.out.println("UUID = " + uuid);
        System.out.println("ARTICLE = " + article);

        System.out.println("FILE = " + file.getOriginalFilename());
        System.out.println("FILE = " + file.getInputStream());
        System.out.println("FILE = " + file.isEmpty());

        if (!file.isEmpty()) {
            imageName = imageUploadService.upload(file);
            article.setThumbnail(imageName);
        }

        articleService.updateByUUID(article);

        return "redirect:" + VIEW_ALL_MAPPING;
    }

    @GetMapping(EDIT_MAPPING + "/{uuid}")
    public String viewEditPage(ModelMap modelMap, @ModelAttribute Article article, @PathVariable("uuid") String uuid) {

        List<Category> categories = categoryService.findAllCategories();
        article = articleService.findByUUID(uuid);

        modelMap.addAttribute("categories", categories);
        modelMap.addAttribute("article", article);
        modelMap.addAttribute("isCreateAction", false);

        return CREATE_PAGE;
    }

    @GetMapping(CREATE_MAPPING)
    public String viewCreatePage(ModelMap modelMap, @ModelAttribute Article article) {

        List<Category> categories = categoryService.findAllCategories();

        modelMap.addAttribute("categories", categories);
        modelMap.addAttribute("article", article);
        modelMap.addAttribute("isCreateAction", true);

        return CREATE_PAGE;
    }

    @PostMapping(CREATE_MAPPING)
    public String create(@ModelAttribute Article article, @RequestParam("file") MultipartFile file) {

        String imageName = imageUploadService.upload(file);

        article.setUuid(UUID.randomUUID().toString());
        article.setThumbnail(imageName);

        System.out.println("ARTICLE = " + article);

        articleService.create(article);

        return "redirect:" + VIEW_ALL_MAPPING;
    }

    @GetMapping(VIEW_ALL_MAPPING)
    public String viewAllPage(ModelMap modelMap) {

        List<Article> articles = articleService.findAll();

        modelMap.addAttribute("articles", articles);

        return VIEW_ALL_PAGE;
    }

}
