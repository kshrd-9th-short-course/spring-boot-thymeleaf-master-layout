package com.chanchhaya.thymeleaflayout.controller.thymeleaf;

import com.chanchhaya.thymeleaflayout.model.User;
import com.chanchhaya.thymeleaflayout.repository.mybatis.UserRepository;
import com.chanchhaya.thymeleaflayout.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class UserThymeleafController {

    private final UserServiceImpl userService;

    @Autowired
    public UserThymeleafController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping("/user/list-data")
    public String listData(ModelMap modelMap) {
        List<User> users = userService.findAllUsers();
        modelMap.addAttribute("users", users);
        return "/fragments/user/user-table";
    }

}
