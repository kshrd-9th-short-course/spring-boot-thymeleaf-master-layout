package com.chanchhaya.thymeleaflayout.controller;

import com.chanchhaya.thymeleaflayout.model.Category;
import com.chanchhaya.thymeleaflayout.service.impl.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CategoryController {

    // Define mapping url
    private final static String LIST_CATEGORIES_URL = "/admin/category";
    private final static String CREATE_CATEGORIES_URL = "/admin/category/create";
    private final static String EDIT_CATEGORY_URL = "/admin/category/edit";
    private final static String DELETE_CATEGORY_URL = "/admin/category/delete";

    // Define path of view template
    private final static String LIST_CATEGORIES_PATH = "admin/category/category-list";
    private final static String CREATE_CATEGORIES_PATH = "admin/category/category-create";

    private final CategoryServiceImpl categoryService;

    @Autowired
    public CategoryController(CategoryServiceImpl categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(DELETE_CATEGORY_URL + "/{id}")
    public String deleteCategory(@PathVariable int id) {

        categoryService.deleteCategoryById(id);

        return "redirect:" + LIST_CATEGORIES_URL;
    }

    @PostMapping(EDIT_CATEGORY_URL)
    public String editCategory(ModelMap modelMap,
                               @ModelAttribute @Valid Category category,
                               BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("ERROR");
            modelMap.addAttribute("category", category);
            modelMap.addAttribute("isEdit", true);
            return CREATE_CATEGORIES_PATH;
        }

        categoryService.updateCategoryById(category);

        return "redirect:" + LIST_CATEGORIES_URL;
    }

    @GetMapping(EDIT_CATEGORY_URL + "/{id}")
    public String editCategoryView(ModelMap modelMap,
                                   @ModelAttribute Category category,
                                   @PathVariable int id) {

        category = categoryService.findCategoryById(id);

        modelMap.addAttribute("category", category);
        modelMap.addAttribute("isEdit", true);

        return CREATE_CATEGORIES_PATH;
    }

    @GetMapping(CREATE_CATEGORIES_URL)
    public String createCategoryView(ModelMap modelMap, @ModelAttribute Category category) {

        modelMap.addAttribute("category", category);

        return CREATE_CATEGORIES_PATH;
    }

    @PostMapping(CREATE_CATEGORIES_URL)
    public String createCategory(@ModelAttribute @Valid Category category, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error");
            return CREATE_CATEGORIES_PATH;
        }

        categoryService.insertCategory(category);

        return "redirect:" + LIST_CATEGORIES_URL;
    }

    @GetMapping(LIST_CATEGORIES_URL)
    public String viewCategory(ModelMap modelMap) {

        List<Category> categories = categoryService.findCategoryAndPrefixHRD();

        modelMap.addAttribute("categories", categories);

        return LIST_CATEGORIES_PATH;
    }
}
