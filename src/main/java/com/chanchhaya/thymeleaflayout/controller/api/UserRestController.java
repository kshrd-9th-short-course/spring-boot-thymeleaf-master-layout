package com.chanchhaya.thymeleaflayout.controller.api;

import com.chanchhaya.thymeleaflayout.model.User;
import com.chanchhaya.thymeleaflayout.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class UserRestController {

    private final UserServiceImpl userService;

    @Autowired
    public UserRestController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @PostMapping("/admin/user/manage/create-new-user")
    public User createNewUser(@RequestBody User user) {
        user.setUuid(UUID.randomUUID().toString());
        user = userService.createNewUser(user);
        System.out.println("USER = " + user);
        return user;
    }

    @GetMapping("/admin/user/manage/find-one-by-uuid")
    public User findOneByUuid(@RequestParam("uuid") String uuid) {
        User user = userService.findOneByUuid(uuid);
        return user;
    }

    @PutMapping("/admin/user/manage/update-by-uuid")
    public User updateExistingUserByUuid(@RequestBody User user) {
        User result = userService.updateExistingUserByUuid(user);
        return result;
    }

    @DeleteMapping("/admin/user/manage/delete-by-uuid/{uuid}")
    public String deleteUserByUuid(@PathVariable("uuid") String uuid) {
        return userService.deleteUserByUuid(uuid);
    }

}
