package com.chanchhaya.thymeleaflayout.controller;

import com.chanchhaya.thymeleaflayout.model.Role;
import com.chanchhaya.thymeleaflayout.model.User;
import com.chanchhaya.thymeleaflayout.service.impl.RoleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class UserController {

    private final RoleServiceImpl roleService;

    @Autowired
    public UserController(RoleServiceImpl roleService) {
        this.roleService = roleService;
    }

    @GetMapping("/admin/user/manage")
    public String viewUserManagePage(ModelMap modelMap) {

        List<Role> roles = roleService.findAllRoles();

        modelMap.addAttribute("roles", roles);

        return "/admin/user/manage";
    }

}
