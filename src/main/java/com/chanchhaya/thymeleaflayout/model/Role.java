package com.chanchhaya.thymeleaflayout.model;

import org.springframework.security.core.GrantedAuthority;

public class Role implements GrantedAuthority {

    /**
     *
     */
    private static final long serialVersionUID = -1612510758516095936L;
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public String getAuthority() {
        return name;
    }

}
