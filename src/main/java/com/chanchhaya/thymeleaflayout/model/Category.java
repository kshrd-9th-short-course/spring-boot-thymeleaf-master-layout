package com.chanchhaya.thymeleaflayout.model;

import javax.validation.constraints.NotBlank;

public class Category {

    private int id;

    @NotBlank(message = "You must fill category name")
    private String name;    // cate_name in database table

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
