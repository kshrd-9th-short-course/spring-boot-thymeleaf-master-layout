package com.chanchhaya.thymeleaflayout.service;

import com.chanchhaya.thymeleaflayout.model.User;

import java.util.List;

public interface UserService {

    User createNewUser(User user);
    List<User> findAllUsers();
    User findOneByUuid(String uuid);
    User updateExistingUserByUuid(User user);
    String deleteUserByUuid(String uuid);

}
