package com.chanchhaya.thymeleaflayout.service.impl;

import com.chanchhaya.thymeleaflayout.model.User;
import com.chanchhaya.thymeleaflayout.repository.mybatis.UserRepository;
import com.chanchhaya.thymeleaflayout.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User createNewUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        boolean isInserted = userRepository.insertUser(user);
        if (isInserted) {
            userRepository.createUserRole(user.getId(), user.getRoles().get(0).getId());
            return user;
        }
        else {
            return null;
        }
    }

    @Override
    public List<User> findAllUsers() {
        return userRepository.selectAllUsers();
    }

    @Override
    public User findOneByUuid(String uuid) {
        return userRepository.selectUserByUuid(uuid);
    }

    @Override
    public User updateExistingUserByUuid(User user) {
        boolean isUpdated = userRepository.updateUserByUuid(user);
        if (isUpdated) {
            userRepository.updateUserRole(user.getId(), user.getRoles().get(0).getId());
            return user;
        } else {
            return null;
        }
    }

    @Override
    public String deleteUserByUuid(String uuid) {
        boolean isDeleted = userRepository.deleteUserByUuid(uuid);
        if (isDeleted) {
            return "User with UUID = " + uuid + " is deleted successfully!";
        } else {
            return "Delete operation is failed!";
        }
    }

}
