package com.chanchhaya.thymeleaflayout.service.impl;

import com.chanchhaya.thymeleaflayout.model.Article;
import com.chanchhaya.thymeleaflayout.repository.mybatis.ArticleRepository;
import com.chanchhaya.thymeleaflayout.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleServiceImpl(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public Article updateByUUID(Article article) {

        boolean isUpdated = articleRepository.updateByUUID(article);

        return isUpdated ? article : null;
    }

    @Override
    public Article findByUUID(String uuid) {
        return articleRepository.selectByUUID(uuid);
    }

    @Override
    public Article create(Article article) {

        boolean isCreated = articleRepository.insert(article);

        return isCreated ? article : null;
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.selectAll();
    }

}
