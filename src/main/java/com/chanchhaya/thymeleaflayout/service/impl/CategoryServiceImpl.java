package com.chanchhaya.thymeleaflayout.service.impl;

import com.chanchhaya.thymeleaflayout.model.Category;
import com.chanchhaya.thymeleaflayout.repository.mybatis.CategoryRepository;
import com.chanchhaya.thymeleaflayout.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAllCategories() {
        return categoryRepository.selectAll();
    }

    @Override
    public List<Category> findCategoryAndPrefixHRD() {

        List<Category> categories = categoryRepository.selectAll();

        for (Category category : categories) {
            category.setName("KSHRD-" + category.getName());
        }

        return categories;
    }

    @Override
    public Category findCategoryById(int id) {
        return categoryRepository.selectById(id);
    }

    @Override
    public void insertCategory(Category category) {
        categoryRepository.insert(category);
    }

    @Override
    public void updateCategoryById(Category category) {
        categoryRepository.updateById(category);
    }

    @Override
    public void deleteCategoryById(int id) {
        categoryRepository.deleteById(id);
    }

}
