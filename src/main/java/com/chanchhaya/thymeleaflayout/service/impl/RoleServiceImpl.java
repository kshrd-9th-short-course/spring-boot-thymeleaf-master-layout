package com.chanchhaya.thymeleaflayout.service.impl;

import com.chanchhaya.thymeleaflayout.model.Role;
import com.chanchhaya.thymeleaflayout.repository.mybatis.RoleRepository;
import com.chanchhaya.thymeleaflayout.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Role> findAllRoles() {
        return roleRepository.selectAllRole();
    }

}
