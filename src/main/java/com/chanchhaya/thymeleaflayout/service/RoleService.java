package com.chanchhaya.thymeleaflayout.service;

import com.chanchhaya.thymeleaflayout.model.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAllRoles();

}
