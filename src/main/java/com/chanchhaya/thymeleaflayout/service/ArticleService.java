package com.chanchhaya.thymeleaflayout.service;

import com.chanchhaya.thymeleaflayout.model.Article;

import java.util.List;

public interface ArticleService {

    Article create(Article article);

    List<Article> findAll();

    Article findByUUID(String uuid);

    Article updateByUUID(Article article);

}
