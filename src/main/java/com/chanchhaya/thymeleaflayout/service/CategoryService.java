package com.chanchhaya.thymeleaflayout.service;

import com.chanchhaya.thymeleaflayout.model.Category;

import java.util.List;

public interface CategoryService {

    List<Category> findAllCategories();

    List<Category> findCategoryAndPrefixHRD();

    Category findCategoryById(int id);

    void insertCategory(Category category);

    void updateCategoryById(Category category);

    void deleteCategoryById(int id);

}
