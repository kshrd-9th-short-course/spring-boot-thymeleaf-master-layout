package com.chanchhaya.thymeleaflayout.configuration.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException {

        String redirectURL = "";

        for (GrantedAuthority authority : authentication.getAuthorities()) {
            if (authority.getAuthority().equals("ROLE_ADMIN")) {
                redirectURL = "/kshrd/admin";
            } else if (authority.getAuthority().equals("ROLE_USER")) {
                redirectURL = "/kshrd/";
            } else {
                redirectURL = "/kshrd/admin/user/manage";
            }
            break;
        }

        httpServletResponse.sendRedirect(redirectURL);

    }

}
