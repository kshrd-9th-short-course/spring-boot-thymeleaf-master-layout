package com.chanchhaya.thymeleaflayout.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException, ServletException {

        String redirectURL = httpServletRequest.getRequestURI();
        System.out.println("REDIRECT_URL = " + redirectURL);

        httpServletRequest.getSession().setAttribute("REDIRECT_SUCCESS_URL", redirectURL);
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/login");

    }

}
