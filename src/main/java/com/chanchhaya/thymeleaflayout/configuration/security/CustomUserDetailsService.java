package com.chanchhaya.thymeleaflayout.configuration.security;

import com.chanchhaya.thymeleaflayout.model.User;
import com.chanchhaya.thymeleaflayout.repository.mybatis.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("USERNAME = " + username);
        User user = userRepository.selectUserByUsername(username);
        return new org.springframework.security.core.userdetails.User(user.getUsername(),
            user.getPassword(), user.getRoles());
    }
    
}
