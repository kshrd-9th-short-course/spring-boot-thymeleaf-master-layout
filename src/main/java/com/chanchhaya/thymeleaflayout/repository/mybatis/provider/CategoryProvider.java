package com.chanchhaya.thymeleaflayout.repository.mybatis.provider;

import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String selectCategoryByIdSQL() {
        return new SQL(){{
            SELECT("*");
            FROM("categories");
            WHERE("id = #{id}");
        }}.toString();
    }

}
