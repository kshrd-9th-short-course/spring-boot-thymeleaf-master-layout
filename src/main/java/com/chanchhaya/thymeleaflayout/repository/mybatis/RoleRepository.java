package com.chanchhaya.thymeleaflayout.repository.mybatis;

import com.chanchhaya.thymeleaflayout.model.Role;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository {

    @Select("SELECT * FROM roles")
    List<Role> selectAllRole();

}
