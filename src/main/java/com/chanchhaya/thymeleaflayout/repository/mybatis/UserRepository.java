package com.chanchhaya.thymeleaflayout.repository.mybatis;

import com.chanchhaya.thymeleaflayout.model.Role;
import com.chanchhaya.thymeleaflayout.model.User;
import com.chanchhaya.thymeleaflayout.repository.mybatis.provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    @InsertProvider(type = UserProvider.class, method = "insertUserSQL")
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    boolean insertUser(User user);

    @SelectProvider(type = UserProvider.class, method = "selectAllUsersSQL")
    @Results(id = "userRoleMapper", value = {
        @Result(property = "roles", column = "id", many = @Many(select = "selectRoleByUserId"))
    })
    List<User> selectAllUsers();

    @InsertProvider(type = UserProvider.class, method = "createUserRoleSQL")
    boolean createUserRole(int userId, int roleId);

    @SelectProvider(type = UserProvider.class, method = "selectRoleByUserIdSQL")
    List<Role> selectRoleByUserId(int userId);

    @Select("SELECT * FROM users WHERE uuid = #{uuid}")
    @ResultMap(value = "userRoleMapper")
    User selectUserByUuid(String uuid);

    @UpdateProvider(type = UserProvider.class, method = "updateUserByUuidSQL")
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    boolean updateUserByUuid(User user);

    @UpdateProvider(type = UserProvider.class, method = "updateUserRoleSQL")
    boolean updateUserRole(int userId, int roleId);

    @Delete("DELETE FROM users WHERE uuid = #{uuid}")
    boolean deleteUserByUuid(String uuid);

    @Select("SELECT * FROM users WHERE username = #{username}")
    @ResultMap(value = "userRoleMapper")
    User selectUserByUsername(String username);

}
