package com.chanchhaya.thymeleaflayout.repository.mybatis.provider;

import org.apache.ibatis.jdbc.SQL;

public class UserProvider {

    public String updateUserRoleSQL() {
        return new SQL() {{
            UPDATE("users_roles");
            SET("role_id = #{roleId}");
            WHERE("user_id = #{userId}");
        }}.toString();
    }

    public String updateUserByUuidSQL() {
        return new SQL() {{
            UPDATE("users");
            SET("username = #{username}");
            SET("email = #{email}");
            SET("password = #{password}");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

    public String selectRoleByUserIdSQL() {
        return new SQL() {{
            SELECT("r.id, r.name");
            FROM("roles r");
            INNER_JOIN("users_roles ur ON ur.role_id = r.id");
            WHERE("ur.user_id = #{userId}");
        }}.toString();
    }

    public String createUserRoleSQL() {
        return new SQL(){{
            INSERT_INTO("users_roles");
            VALUES("user_id", "#{userId}");
            VALUES("role_id", "#{roleId}");
        }}.toString();
    }

    public String insertUserSQL() {
        return new SQL() {{
            INSERT_INTO("users");
            VALUES("uuid", "#{uuid}");
            VALUES("username", "#{username}");
            VALUES("email", "#{email}");
            VALUES("password", "#{password}");
        }}.toString();
    }

    public String selectAllUsersSQL() {
        return new SQL() {{
            SELECT("*");
            FROM("users");
            WHERE("status = true");
            ORDER_BY("id DESC");
        }}.toString();
    }

}
