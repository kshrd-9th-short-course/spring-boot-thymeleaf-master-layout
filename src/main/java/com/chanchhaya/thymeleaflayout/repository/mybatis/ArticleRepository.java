package com.chanchhaya.thymeleaflayout.repository.mybatis;

import com.chanchhaya.thymeleaflayout.model.Article;
import com.chanchhaya.thymeleaflayout.model.Category;
import com.chanchhaya.thymeleaflayout.repository.mybatis.provider.ArticleProvider;
import com.chanchhaya.thymeleaflayout.repository.mybatis.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {

    @SelectProvider(type = ArticleProvider.class, method = "selectByUUIDSQL")
    @ResultMap(value = "articleMapper")
    Article selectByUUID(String uuid);

    @InsertProvider(type = ArticleProvider.class,
            method = "insertSQL")
    boolean insert(Article article);

    @SelectProvider(type = ArticleProvider.class, method = "selectAllSQL")
    @Results(id = "articleMapper", value = {
            @Result(property = "category", column = "category_id", one = @One(select = "selectCategoryById"))
    })
    List<Article> selectAll();

    @SelectProvider(type = CategoryProvider.class, method = "selectCategoryByIdSQL")
    @Results(id = "categoryMapper", value = {
            @Result(property = "name", column = "cate_name")
    })
    Category selectCategoryById(int id);

    @UpdateProvider(type = ArticleProvider.class, method = "updateByUUIDSQL")
    boolean updateByUUID(Article article);

}
