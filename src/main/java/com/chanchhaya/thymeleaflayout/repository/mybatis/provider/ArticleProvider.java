package com.chanchhaya.thymeleaflayout.repository.mybatis.provider;

import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {

    public String updateByUUIDSQL() {
        return new SQL(){{
            UPDATE("articles");
            SET("title = #{title}");
            SET("description = #{description}");
            SET("thumbnail = #{thumbnail}");
            SET("content = #{content}");
            SET("category_id = #{category.id}");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

    public String selectByUUIDSQL() {
        return new SQL() {{
            SELECT("*");
            FROM("articles");
            WHERE("uuid = #{uuid}");
            AND();
            WHERE("status = TRUE");
        }}.toString();
    }

    public String selectAllSQL() {
        return new SQL(){{
            SELECT("*");
            FROM("articles");
            WHERE("status = TRUE");
            ORDER_BY("id DESC");
        }}.toString();
    }

    public String insertSQL() {
        return new SQL(){{
            /* TODO ==> Define SQL statement */
            INSERT_INTO("articles");
            VALUES("uuid", "#{uuid}");
            VALUES("title", "#{title}");
            VALUES("description", "#{description}");
            VALUES("thumbnail", "#{thumbnail}");
            VALUES("content", "#{content}");
            VALUES("category_id", "#{category.id}");
        }}.toString();
    }

}
