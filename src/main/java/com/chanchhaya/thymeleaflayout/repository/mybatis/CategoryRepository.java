package com.chanchhaya.thymeleaflayout.repository.mybatis;

import com.chanchhaya.thymeleaflayout.model.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @Insert("INSERT INTO categories (cate_name) VALUES (#{name})")
    boolean insert(Category category);

    @Select("SELECT * FROM categories ORDER BY id DESC")
    @Results(id = "categoryMapper", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "cate_name")
    })
    List<Category> selectAll();

    @Select("SELECT * FROM categories WHERE id = #{id}")
    @ResultMap(value = "categoryMapper")
    Category selectById(int id);

    @Delete("DELETE FROM categories WHERE id = #{id}")
    boolean deleteById(int id);

    @Update("UPDATE categories SET cate_name = #{name} WHERE id = #{id}")
    boolean updateById(Category category);

}
