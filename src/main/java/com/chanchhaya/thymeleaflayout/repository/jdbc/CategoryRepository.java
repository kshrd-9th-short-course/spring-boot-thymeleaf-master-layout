package com.chanchhaya.thymeleaflayout.repository.jdbc;

import com.chanchhaya.thymeleaflayout.model.Category;

import java.util.List;

public interface CategoryRepository {

    List<Category> findAll();

}
