package com.chanchhaya.thymeleaflayout.testcontroller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BasicController {

    @GetMapping("/basic")
    public String basicView(ModelMap modelMap) {
        modelMap.addAttribute("username", "Sok Polen");
        return "/admin/testing/basic";
    }

}
